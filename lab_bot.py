import discord
import asyncio
import logging
from openpyxl import load_workbook
from framefind import lookUpSheetName, findB_col, findrow

logging.basicConfig(level=logging.INFO)

#importing workbook
wb = load_workbook('SFVFrameData.xlsx')

client = discord.Client()

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('------')

@client.event
async def on_message(message):
    if message.content.startswith('!data'):
        msg = message.content[6:]
        msg = tuple(msg.split())
        newMsg = findrow(findB_col(lookUpSheetName(msg, wb)))
        await client.send_message(message.channel, newMsg)
    if message.content.startswith('!test'):
        counter = 0
        tmp = await client.send_message(message.channel, 'Calculating messages...')
        async for log in client.logs_from(message.channel, limit=100):
            if log.author == message.author:
                counter += 1

        await client.edit_message(tmp, 'You have {} messages.'.format(counter))
    elif message.content.startswith('!sleep'):
        await asyncio.sleep(5)
        await client.send_message(message.channel, 'Done sleeping')

    if message.author == client.user:
            return

    if message.content.startswith('!hello'):
        msg = 'Hello {0.author.mention}'.format(message)
        await client.send_message(message.channel, msg)

client.run('MjQxNjcwNTI3NjgzNTI2NjY2.CvfhSw.Qmff-kzAvm48OJumS1yPFmmOmdw')
