# finding frames module
from openpyxl import load_workbook
#remeber to load in a sheet object for this function
def lookUpSheetName(inputls, wb): #look for the sheet from the input given
	sheetList = list()
	for i in wb.sheetnames:
		sheetList.append(wb[i])
	del sheetList[:2]
	sfDude = ' '
	for i in sheetList:
		if inputls[0] in str(i):
			print(i)
			sfDude=i
	if str(sfDude) != ' ':
		return sfDude, str(inputls[1].upper())
	else:
		print(sfDude)#raise an exception
		return 'Nothing found'

def findB_col(inputls):
	col_b = list()
	n = 0
	for i in inputls[0]['B']:
		if i.value != None:
			col_b.append(i.value)
			n = n +1
		elif n < 100:
			n = n +1
		else:
			return col_b, inputls[0], inputls[1]
		    
def findrow(findbFunc): #taking in a tuple that contains three items
    col_b = findbFunc[0]
    sheet = findbFunc[1]
    move = findbFunc[2]
    tempResult = list()
    header = list()
    pos = ' '
    for commands in col_b:
        if str(move) == commands:
            pos = col_b.index(move) + 1
            
    if pos == ' ':
        print("nothing found") # give result an input or raise flag
    else:      #max_col here should be a global constant of 20
        for i in sheet.iter_rows(min_row=pos, max_row=pos, max_col=20):
            for cell in i:
                tempResult.append(cell.value)
        for i in sheet.iter_rows(min_row=1, max_row=1, max_col=20):
            for cell in i: #This loop should be a static global I'm lazy atm
                header.append(cell.value)
    result = tuple(zip(header, tempResult))
    return result
